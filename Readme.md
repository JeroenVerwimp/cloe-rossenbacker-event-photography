Cloe Rossenbacker Event Photography
====================

A gallery website for Cloe Rossenbacker Event Photography.

## Features
+ Photo albums
+ Contact
+ about me
+ Home page display
+ Facebook integration
+ Admin panel
+ calender

## Pages
+ Home      /home
+ Gallery   /gallery
+ Contact   /contact
+ Admin     /admin
+ About me   /about

## Extra
+ Language: English


## CURRENT TODO:
+ Change controller and view import to core/init
+ Add language support
+ Add helpers (database, uploader, password)



// RewriteRule .* index.php?$0 [PT,L]