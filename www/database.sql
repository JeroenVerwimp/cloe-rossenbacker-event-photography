-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Gegenereerd op: 08 jul 2015 om 01:17
-- Serverversie: 5.5.31-log
-- PHP-versie: 5.4.42

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jeroenve_cloe`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `albums`
--

CREATE TABLE IF NOT EXISTS `albums` (
  `id` int(8) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` varchar(255) NOT NULL,
  `thumb_image` bigint(8) NOT NULL DEFAULT '0',
  `images` text NOT NULL,
  `hidden` int(1) NOT NULL DEFAULT '0',
  `deletable` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `albums`
--

INSERT INTO `albums` (`id`, `name`, `description`, `thumb_image`, `images`, `hidden`, `deletable`) VALUES
(1, 'Carousel', '', 154, '[154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186]', 1, 0),
(34, 'Daydream festival 2015 (zaterdag)', '', 0, '[154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186]', 0, 1),
(37, 'The great VATsby', '', 0, '[]', 0, 1),
(38, 'Booze ''n Bass', '', 0, '[]', 0, 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `hit_hits`
--

CREATE TABLE IF NOT EXISTS `hit_hits` (
  `id` int(11) NOT NULL,
  `pageid` varchar(100) NOT NULL,
  `isunique` tinyint(1) NOT NULL,
  `hitcount` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `hit_hits`
--

INSERT INTO `hit_hits` (`id`, `pageid`, `isunique`, `hitcount`) VALUES
(1, 'home', 0, 93),
(2, 'home', 1, 49),
(3, 'about', 0, 14),
(4, 'about', 1, 9),
(5, 'gallery', 0, 26),
(6, 'gallery', 1, 12),
(7, 'contact', 0, 10),
(8, 'contact', 1, 6);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `hit_nodupes`
--

CREATE TABLE IF NOT EXISTS `hit_nodupes` (
  `ids_hash` char(64) NOT NULL,
  `time` bigint(20) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `hit_nodupes`
--

INSERT INTO `hit_nodupes` (`ids_hash`, `time`) VALUES
('15af794c86a5cb15d1e56b44748cb9ca31b52b729618e394c62ab2c558f14b4b', 1433876238),
('17861fe0c38fa309e13ca095bca6e0234dab7275f1f9a6e5d738a29bdca83c37', 1433876238),
('549609dcf36b1874f3a52f88ca6f1261f3f7171c8cdd213e28d552344b98996b', 1434297173),
('5546af3411027997fd67d6d79879b60796200ac97051d2b28e65cf490fc0ad33', 1434232171),
('608475e84edd56b4d6087413498c41683039798cae3ac97d19e8f0b3e1206575', 1435085811),
('685151940042ac93fb825fb85d1a100fff4ee3e467b2ae5c40d1c0fb8823b4a0', 1434102065),
('6a716611a4bef5869116bc0f109b53c01d7c42d61ccfef463661e0696edf5352', 1435218465),
('7f38682b93ca3d91083241e0d62c8eb9f271d1c16297467628feab715c89e060', 1434102121),
('845d38f8cb7bf69079f34e6a5084fc2de044962a066e7ef511a40c3e99eaf83a', 1434481002),
('972b546cc8ec7157ba87370e7af7b3db86b8ed8c962b0dd3cda001242c6fdd4b', 1435690587),
('9cb881624b44c047add17deb0b05616453fb83be2c15bc4200fd3b7a5cb46747', 1434295337),
('aedd125cb40f18ffd613ce6d80968df27dd4ada74382ea2e6bb0cfd4d2423d93', 1436309702),
('b5be0558407cf9fa692428de79a3f05ed68522107bbb5c640c0349f34ae80c9c', 1434232244),
('cf279f4cb5e1cc22062cca41ddd68f5d1c01a07e7bc7426906bd716c3abe4eb7', 1434480999),
('d901acdc4861d799ff892afdbb0abb36d930e6cfaccf65da5a0481a77cdefdf2', 1436309600),
('f412f7ef4fca88af5d9ec8774d2eb78f7592864c7bb19ec9aeb146001fa8d243', 1435140480),
('f7960c489e60c01106cfc6fae5498425d0b663abb0701a020eea0bd3d62a7561', 1435148850);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` bigint(8) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `hidden` int(1) NOT NULL DEFAULT '0',
  `deletable` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=362 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `images`
--

INSERT INTO `images` (`id`, `name`, `description`, `url`, `hidden`, `deletable`) VALUES
(154, 'unknown', 'unknown', 'img/uploads/55590253739e92.51485465.jpg', 0, 1),
(155, 'unknown', 'unknown', 'img/uploads/5559025418f454.57854762.jpg', 0, 1),
(156, 'unknown', 'unknown', 'img/uploads/55590254788502.07538690.jpg', 0, 1),
(157, 'unknown', 'unknown', 'img/uploads/55590254d165c1.50858533.jpg', 0, 1),
(158, 'unknown', 'unknown', 'img/uploads/555902553f8ef7.37199465.jpg', 0, 1),
(159, 'unknown', 'unknown', 'img/uploads/5559025597dba9.39027774.jpg', 0, 1),
(160, 'unknown', 'unknown', 'img/uploads/55590256018fa9.95013613.jpg', 0, 1),
(161, 'unknown', 'unknown', 'img/uploads/555902566f73c3.14552282.jpg', 0, 1),
(162, 'unknown', 'unknown', 'img/uploads/55590256c1aef5.51712185.jpg', 0, 1),
(163, 'unknown', 'unknown', 'img/uploads/55590257298735.44569340.jpg', 0, 1),
(164, 'unknown', 'unknown', 'img/uploads/5559025788e652.84297232.jpg', 0, 1),
(165, 'unknown', 'unknown', 'img/uploads/55590257eeed70.71167625.jpg', 0, 1),
(166, 'unknown', 'unknown', 'img/uploads/555902585eb6c2.24498700.jpg', 0, 1),
(167, 'unknown', 'unknown', 'img/uploads/55590258c4d781.16723802.jpg', 0, 1),
(168, 'unknown', 'unknown', 'img/uploads/555902dc3d7b12.96737436.jpg', 0, 1),
(169, 'unknown', 'unknown', 'img/uploads/555902dcc9bc89.79072465.jpg', 0, 1),
(170, 'unknown', 'unknown', 'img/uploads/555902dd303c63.35554809.jpg', 0, 1),
(171, 'unknown', 'unknown', 'img/uploads/555902dda90e30.48760548.jpg', 0, 1),
(172, 'unknown', 'unknown', 'img/uploads/555902de0b4446.51517742.jpg', 0, 1),
(173, 'unknown', 'unknown', 'img/uploads/555902de6c7c79.72638960.jpg', 0, 1),
(174, 'unknown', 'unknown', 'img/uploads/555902deea3016.71066953.jpg', 0, 1),
(175, 'unknown', 'unknown', 'img/uploads/555902df53f5c0.02805950.jpg', 0, 1),
(176, 'unknown', 'unknown', 'img/uploads/555902dfbb1f43.77853032.jpg', 0, 1),
(177, 'unknown', 'unknown', 'img/uploads/555902e02bf369.81951945.jpg', 0, 1),
(178, 'unknown', 'unknown', 'img/uploads/555902e0899846.29273604.jpg', 0, 1),
(179, 'unknown', 'unknown', 'img/uploads/555902e0e43632.62471872.jpg', 0, 1),
(180, 'unknown', 'unknown', 'img/uploads/555902e149d275.08773416.jpg', 0, 1),
(181, 'unknown', 'unknown', 'img/uploads/555902e1b012c3.54535655.jpg', 0, 1),
(182, 'unknown', 'unknown', 'img/uploads/555902e21509f1.40948583.jpg', 0, 1),
(183, 'unknown', 'unknown', 'img/uploads/555902e26e1ee7.45510559.jpg', 0, 1),
(184, 'unknown', 'unknown', 'img/uploads/555902e2c50f48.48494017.jpg', 0, 1),
(185, 'unknown', 'unknown', 'img/uploads/555902e32473f6.50013976.jpg', 0, 1),
(186, 'unknown', 'unknown', 'img/uploads/555902e3848812.49305179.jpg', 0, 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `name` varchar(64) NOT NULL,
  `type` varchar(32) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `settings`
--

INSERT INTO `settings` (`name`, `type`, `value`) VALUES
('contact_forward', 'boolean', '1'),
('contact_forward_email', 'email', 'jeroen.verwimp@gmail.com'),
('page_about', 'text', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum hic, placeat repellendus voluptas unde enim sequi necessitatibus nihil quia praesentium, aliquam fugiat veritatis minima omnis aliquid distinctio quidem iure <a href="http://google.be" onclick="window.open(this.href, ''test'', ''resizable=no,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=no,dependent=no''); return false;">doloribus</a>!</p><p>&nbsp;</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum hic, placeat repellendus voluptas unde enim sequi necessitatibus nihil quia praesentium, aliquam fugiat veritatis minima omnis aliquid distinctio quidem iure doloribus!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum hic, placeat repellendus voluptas unde enim sequi necessitatibus nihil quia praesentium, aliquam fugiat veritatis minima omnis aliquid distinctio quidem iure doloribus!</p>'),
('share_facebook', 'boolean', '1'),
('share_googleplus', 'boolean', '1'),
('share_twitter', 'boolean', '1');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(8) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(16) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `date_created`, `date_updated`) VALUES
(1, 'superuser', '$2up1ouhoq0ko', '2015-03-17 22:04:15', '2015-03-17 22:04:15'),
(2, 'cloe', '$2z9ps8u2FHDw', '2015-03-17 22:13:39', '2015-03-17 22:13:39');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexen voor tabel `hit_hits`
--
ALTER TABLE `hit_hits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pageid` (`pageid`);

--
-- Indexen voor tabel `hit_nodupes`
--
ALTER TABLE `hit_nodupes`
  ADD PRIMARY KEY (`ids_hash`);

--
-- Indexen voor tabel `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`name`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexen voor tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `albums`
--
ALTER TABLE `albums`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT voor een tabel `hit_hits`
--
ALTER TABLE `hit_hits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT voor een tabel `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=362;
--
-- AUTO_INCREMENT voor een tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
