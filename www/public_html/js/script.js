$(function() {

  

  $('[data-jcarousel]').each(function() {
    var el = $(this);
    el.jcarousel(el.data()).jcarouselAutoscroll({
      interval: 3000,
      target: '+=1',
      autostart: true
    });
  });

  $('[data-jcarousel-control]').each(function() {
    var el = $(this);
    el.jcarouselControl(el.data());
  });

  $('.contact-form .input-group input').focusout(function() {
    var text_val = $(this).val();
    
    if(text_val === "") {
      $(this).removeClass('has-value');
    } else {
      $(this).addClass('has-value');
    }
  });

  $('.contact-form .input-group textarea').focusout(function() {
    var text_val = $(this).val();
    
    if(text_val === "") {
      $(this).removeClass('has-value');
    } else {
      $(this).addClass('has-value');
    }
  });
  
  $(window).load(function() {
    $('.album-carousel').height($('.album-carousel').width() / 3 * 2);
    $('.album-carousel-image-container').width($('.album-carousel').width());
    $('.album-carousel-image-container').height($('.album-carousel').height());
    
    $('.album-carousel-image-container').each(function(key,value) {
      if($(value).children('img').width() > $(value).children('img').height()) {
        $(value).children('img').width($(value).width());
      } else {
        $(value).children('img').height($('.album-carousel').height());
      }
    });
    
    $('.album-carousel-main').css("margin-top", (($(window).height() / 2) - ($('.album-carousel-main').height() / 2)) + "px");
  });

  $(window).resize(function(){
    $('.album-carousel').height($('.album-carousel').width() / 3 * 2);
    $('.album-carousel-image-container').width($('.album-carousel').width());
    $('.album-carousel-image-container').height($('.album-carousel').height());
    
    $('.album-carousel-image-container').each(function(key,value) {
      if($(value).children('img').width() > $(value).children('img').height()) {
        $(value).children('img').width($(value).width());
      } else {
        $(value).children('img').height($('.album-carousel').height());
      }
    });
    
    $('.album-carousel-main').css("margin-top", (($(window).height() / 2) - ($('.album-carousel-main').height() / 2)) + "px");
  });
  
  $('.thumb-unit').on('click', function(e){
    e.preventDefault();
    
    $('.album-carousel .images').css('margin-left', '0px');
    
    var data = {
      'action': 'albumData',
      'albumId': parseInt($(this).data('id'))
    };
    
    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: 'admin/ajax',
      data: data,
      success: function(data) {
        data = jQuery.parseJSON( data["json"] );
        
        $('.album-carousel .images').empty();
        
        var images = data.albumImages;
        for(var i = 0; i < images.length; i++) {
          $('.album-carousel .images').append('<li><div class="album-carousel-image-container"><img src="../' + images[i].url + '" alt="" class="album-carousel-image"></div></li>');
        }
        
        $('.album-carousel').height($('.album-carousel').width() / 3 * 2);
        $('.album-carousel-image-container').width($('.album-carousel').width());
        $('.album-carousel-image-container').height($('.album-carousel').height());

        $('.album-carousel-image-container').each(function(key,value) {
          if($(value).children('img').width() > $(value).children('img').height()) {
            $(value).children('img').width($(value).width());
          } else {
            $(value).children('img').height($('.album-carousel').height());
          }
        });
        
        $('.gallery-album-wrapper h1').text(data.albumData.name);
        $('.gallery-page-wrapper').addClass('show');
      },
      error: function(request, status, error){
        console.log('error: ' + request.responseText);
      }
    });
  });
  
  $('.gallery-album-wrapper .back').on('click', function(e) {
    e.preventDefault();
    $('.gallery-page-wrapper').removeClass('show');
  });
  
  var moving = false;
  
  $('.album-carousel .prev').on('click', function( e ) {
    e.preventDefault();
    
    if(moving === false) {
      moving = true;
      
      $('.album-carousel .images').prepend($('.album-carousel .images li').last());
      $('.album-carousel .images').css('margin-left', '-=' + $('.album-carousel-image-container').width());
      
      setTimeout(function (){
        $('.album-carousel .images').addClass('trans');
        $('.album-carousel .images').css('margin-left', '0px');
      }, 1);
      
      setTimeout(function (){
        moving = false;
        $('.album-carousel .images').removeClass('trans');
        
      }, 550);
    }
  });
  
  $('.album-carousel .next').on('click', function( e ) {
    e.preventDefault();
    
    if(moving === false) {
      moving = true;
      
      $('.album-carousel .images').addClass('trans');
      $('.album-carousel .images').css('margin-left', '-=' + $('.album-carousel-image-container').width());
      
      setTimeout(function (){
        moving = false;
        $('.album-carousel .images').removeClass('trans');
        $('.album-carousel .images').css('margin-left', '0px');
        $('.album-carousel .images').append($('.album-carousel .images li').first());
      }, 550);
    }
  });
  
});











































