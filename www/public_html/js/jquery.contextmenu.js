(function ( $ ) {

  $.fn.contextmenu = function( options ) {
    var container = $(this);
    var opts = $.extend({}, $.fn.contextmenu.defaults, options);
    
    $(this).bind('contextmenu', function( e ) {
      e.preventDefault();
      
      $(opts.selector).css({
        display: 'block',
        top: (event.pageY + 1)+ 'px',
        left: (event.pageX - 220) + 'px'
      });
    });
    
    $(document).bind('mousedown', function(e) {
      if(!($(e.target).parents(opts.selector).length > 0)) {
        $(opts.selector).css('display', 'none');
      } else {
        var action = $(e.target).parents('li').data('action');
        if(typeof action != 'undefined') {
          $(opts.selector).css('display', 'none');
          opts.callback(action, container);
        }
      }
    });
    
    $(document).bind('keydown', function(e) {
      if(e.keyCode == 27) {
        if($(opts.selector).css('display') === 'block') {
          e.preventDefault();
          $(opts.selector).css('display', 'none');
        } else {
          $('[id^=checkbox-image-]').prop('checked', false);
        }
      }
    });
    
    
  };
  
  $.fn.contextmenu.defaults = {
    selector: '.html5-context-menu',
    callback: function(key, element) {
      console.log(key);
    }
  };
  
}( jQuery ));