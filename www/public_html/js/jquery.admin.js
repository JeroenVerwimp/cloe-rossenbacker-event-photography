$(function() {
  
  $('.admin-content').mCustomScrollbar({
    axis: "y",
    theme: "minimal-dark",
    mouseWheel: {
      scrollAmount: 300000
    }
  });
  
  $('.btn-addalbum').on('click', function (e) {
    e.preventDefault();
    if ($('.modal-addalbum').hasClass('hidden')) {
      $('.modal-addalbum').removeClass('hidden');
    }
    
    $('#modal-addalbum .images .images1,.images2').remove();
    $('#modal-addalbum .images').append('<div class="images1"></div><div class="images2"></div>');
    
    $('#modal-addalbum-data-id').val('');
    $('#modal-addalbum-data-name').val("");
    $('#modal-addalbum-data-hidden').prop('checked', false);
    
    var data = {
      'action': 'allImages'
    };
    
    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: 'ajax',
      data: data,
      success: function(data) {
        data = jQuery.parseJSON( data["json"] );
        
        $('#modal-addalbum .images .images1,.images2').mCustomScrollbar({
          axis: "y",
          theme: "minimal-dark"
        });
        
        var images = data.images;
        for(var i = 0; i < images.length; i++) {
          $('#modal-addalbum .images .images1 .mCustomScrollBox .mCSB_container').append('<div class="image" data-image-id="' + images[i].id + '"><label><input id="images-' + images[i].id + '" type="checkbox"><div class="overlay"></div><img src="../' + images[i].url +'" alt=""></label></div>');
        }

        
      },
      error: function(request, status, error){
        console.log('error: ' + request.responseText);
      }
    });
    
  });
  
  $('.modal-addalbum-exit').on('click', function (e) {
    $('.modal-addalbum').addClass('hidden');
  });
  
  $('#upload-add-to-album').on('change', function () {
    if ($(this).prop('checked')) {
      $('#selectalbum').prop('disabled', false);
    } else {
      $('#selectalbum').prop('disabled', true);
      $('#selectalbum').val('Carousel');
      $('#upload-to-album-info').css('display', 'none');
    }
  });
  
  $('#selectalbum').on('change', function () {
    if (this.value === '4586_createalbum') {
      $('#upload-to-album-info').css('display', 'initial');
    } else {
      $('#upload-to-album-info').css('display', 'none');
    }
  });
  
  
  
  
  
  var dropzone = $('#dropzone');
  var upload_button = $('#dropzone_upload');
  var select_manual = $('#dropzone_select_manual');
  var formData = new FormData();
  var toAlbum = false;

  $('#modal-upload-exit').on('click', function( e ) {
    formData = new FormData();
    
    if($('#dropzone:last-child').hasClass("upload-file")) {
      while ($('#dropzone:last-child').hasClass("upload-file")) {
        $('#dropzone:last-child').remove();
      }
    }

    $('#dropzone_title').css( 'display', 'block' );
    $('#modal-upload').addClass('hidden');
  });

  $('#dropzone_upload').on('click', function (e) {
    var xhr = new XMLHttpRequest;
    
    $('.upload-overlay').addClass('show');
    
    if ($('#upload-add-to-album').is(":checked")) {
      toAlbum = true;
      if ($('#selectalbum').val() == '4586_createalbum') {
        var name = $('#upload-to-album-name').val();
        
        formData.append('createAlbum', true);
        formData.append('albumName', name);
      } else {
        var id = $('#selectalbum').val();
        
        formData.append('albumName', $("option[value='" + id + "']").text())
        formData.append('albumId', id);
      }
    }
    formData.append('toAlbum', toAlbum);
    
//    xhr.open('post', 'galleryuploadtoalbum');
//    xhr.send(formData);
    $.ajax({
      url: "galleryuploadtoalbum",
      data: formData,
      processData: false,
      contentType: false,
      type: 'POST',
      success: function ( data ) {
        console.log( data );
        formData = new FormData();
        
        $('.upload-overlay').removeClass('show');
      },
      error: function(request, status, error){
        console.log('error: ' + request.responseText);
        formData = new FormData();
        
        $('.upload-overlay').removeClass('show');
        location.reload();
      }
    });
    
    $('#dropzone').children('.upload-file').remove();
    $('#dropzone_title').css( 'display', 'block' );
    $('#modal-upload').addClass('hidden');
    //location.reload(true);
  });
  
  var addFiles = function(files) {
    var x;
    $('#dropzone_title').css( 'display', 'none' );
    
    
    for (x = 0; x < files.length; x = x + 1) {
      formData.append('file[]', files[x]);

      var list_item = document.createElement('div');
      var list_item_img = document.createElement('img');
      var list_item_title = document.createElement('span');

      list_item.className = "upload-file"
      list_item_img.src = "../img/photo-icon.png"
      list_item_title.innerText = files[x].name;

      list_item.appendChild(list_item_img);
      list_item.appendChild(list_item_title);
      $('#dropzone').append(list_item);
    }
  };

  $('#dropzone').on('drop', function( e ) {
    e.preventDefault();
    
    if($(this).hasClass('dragover')) {
      $(this).removeClass('dragover');
    }
    
    addFiles(e.originalEvent.dataTransfer.files);
  });

  $('#dropzone').on('dragover', function( e ) {
    if(!$(this).hasClass('dragover')) {
      $(this).addClass('dragover');
    }
    return false;
  });

  $('#dropzone').on('dragleave', function( e ) {
    if($(this).hasClass('dragover')) {
      $(this).removeClass('dragover');
    }
    return false;
  });

  $('#dropzone_select_manual').on('change', function(e) {
    addFiles(e.originalEvent.target.files);
  });
  
  
  
  
  
  
  $(document).bind('keydown', function(e) {
    if(e.ctrlKey && e.keyCode == 'A'.charCodeAt(0)) {
      e.preventDefault();
      $('[id^=checkbox-image-]').prop('checked', true);
    } else if(e.keyCode == 46) {
      deleteimages();
    } else if(e.keyCode == 27) {
      $('[id^=checkbox-image-]').prop('checked', false);
    }
  });
  
  $('.btn-delete').on('click', function(e) {
    e.preventDefault();
    deleteimages();
  });
  
  function deleteimages() {
    if($('[id^=checkbox-image-]:checked').length === 0) {
      alert('no pictures selected!!');
      return;
    }
    var yes = window.confirm("Are you sure you want to delete the selected pictures?");
    if(yes === false) {
      return;
    }

    var ids = new Array();
    $('[id^=checkbox-image-]:checked').each(function(index) {
      var id = $(this).parent().parent().data('imageId');
      ids.push(parseInt(id));
    });
    
    var data = {
      'action': 'deleteImages',
      'imageIds': JSON.stringify(ids)
    };
    
    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: 'ajax',
      data: data,
      success: function(data) {
        data = jQuery.parseJSON( data["json"] );
        location.reload();
      },
      error: function(request, status, error){
        console.log('error: ' + request.responseText);
      }
    });
    
    $('[id^=checkbox-image-]').prop('checked', false);
  }
  
  $('.images1').mCustomScrollbar({ //#modal-addalbum form .images 
    axis: "y",
    theme: "minimal-dark"
  });
  
  $('.btn-album-edit').on('click', function(e) {
    e.preventDefault();
    
    var data = {
      'action': 'albumData',
      'albumId': parseInt($(this).data('albumId'))
    };
    
    $('#modal-addalbum .images .images1,.images2').remove();
    $('#modal-addalbum .images').append('<div class="images1"></div><div class="images2"></div>');
    
    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: 'ajax',
      data: data,
      success: function(data) {
        data = jQuery.parseJSON( data["json"] );
        
        $('#modal-addalbum .images .images1,.images2').mCustomScrollbar({
          axis: "y",
          theme: "minimal-dark"
        });
        
        var unusedImages = data.unusedImages;
        for(var i = 0; i < unusedImages.length; i++) {
          $('#modal-addalbum .images .images1 .mCustomScrollBox .mCSB_container').append('<div class="image" data-image-id="' + unusedImages[i].id + '"><label><input id="images-' + unusedImages[i].id + '" type="checkbox"><div class="overlay"></div><img src="../' + unusedImages[i].url +'" alt=""></label></div>');
        }
        
        var images = data.albumImages;
        for(var i = 0; i < images.length; i++) {
          $('#modal-addalbum .images .images2 .mCustomScrollBox .mCSB_container').append('<div class="image" data-image-id="' + images[i].id + '"><label><input id="images-' + images[i].id + '" type="checkbox"><div class="overlay"></div><img src="../' + images[i].url +'" alt=""></label></div>');
        }
        
        $('#modal-addalbum-data-id').val(data.albumId);
        $('#modal-addalbum-data-name').val(data.albumData.name);
        $('#modal-addalbum-data-name').prop('disabled', (data.albumData.id === 1 ? true : false));
        $('#modal-addalbum-data-hidden').prop('checked', (data.albumData.hidden === 1 ? true : false));
        $('#modal-addalbum-data-hidden').prop('disabled', (data.albumData.id === 1 ? true : false));
        
        
        $('.images1,.images2 .image').each(function() {
          if($(this).data('imageId') == data.albumData.thumb_image) {
            $(this).addClass('thumb');
          }
        });
        
        if ($('.modal-addalbum').hasClass('hidden')) $('.modal-addalbum').removeClass('hidden');
        
      },
      error: function(request, status, error){
        console.log('error: ' + request.responseText);
      }
    });
    
    return false;
  });
  
  $('.move-container .move-right').on('click', function(e) {
    e.preventDefault();
    $('[id^=images-]:checked').each(function(index) {
      $(this).parent().parent().appendTo('#modal-addalbum .images .images2 .mCustomScrollBox .mCSB_container');
      $(this).prop('checked', false);
    });
  });
  
  $('.move-container .move-left').on('click', function(e) {
    e.preventDefault();
    $('[id^=images-]:checked').each(function(index) {
      $(this).parent().parent().appendTo('#modal-addalbum .images .images1 .mCustomScrollBox .mCSB_container');
      $(this).prop('checked', false);
    });
  });
  
  $('#modal-addalbum form #btn-save').on('click', function(e) {
    e.preventDefault();
    
    var images = [];
    $('#modal-addalbum .images .images2 .mCustomScrollBox .mCSB_container .image').each(function() {
      var image_id = $(this).data('imageId');
      images.push(image_id);
    });
    
    var albumThumb = 0;
    if($('.thumb').length) {
      albumThumb = $('.thumb').first().data('imageId');
    }
    
    var data = {
      'action': 'saveAlbumData',
      'albumId': $('#modal-addalbum-data-id').val(),
      'albumName': $('#modal-addalbum-data-name').val(),
      'albumHidden': $('#modal-addalbum-data-hidden').prop('checked'),
      'albumImages': JSON.stringify(images),
      'albumThumb': albumThumb
    };
    
    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: 'ajax',
      data: data,
      success: function(data) {
        data = jQuery.parseJSON( data["json"] );
        //console.log(data);
      },
      error: function(request, status, error){
        console.log('error: ' + request.responseText);
      }
    });
    
    if (!$('.modal-addalbum').hasClass('hidden')) $('.modal-addalbum').addClass('hidden');
  });
  
  $('#modal-addalbum form #btn-album-delete').on('click', function(e) {
    e.preventDefault();
    
    var data = {
      'action': 'deleteAlbum',
      'albumId': $('#modal-addalbum-data-id').val()
    };
    
    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: 'ajax',
      data: data,
      success: function(data) {
        data = jQuery.parseJSON( data["json"] );
        console.log(data);
        location.reload();
      },
      error: function(request, status, error){
        console.log('error: ' + request.responseText);
      }
    });
    
    if (!$('.modal-addalbum').hasClass('hidden')) $('.modal-addalbum').addClass('hidden');
  });
  
  $('#btn-make-thumb').on('click', function(e) {
    e.preventDefault();
    
    if($('[id^=images-]:checked').length === 0) {
      alert('no pictures selected!!');
      return;
    }

    $('[id^=images-]:checked').each(function(index) {
      $('.images1,.images2 .image').removeClass('thumb');
      $(this).parent().parent().addClass('thumb');
    });
  });
  
  if( $('#aboutText').length ) {
    CKEDITOR.disableAutoInline = true;
    var editor = CKEDITOR.inline(document.getElementById('aboutText'));
    $('.btn-save-about').on('click', function(e) {
      e.preventDefault();

      var aboutText = editor.getData();

      var data = {
        'action': 'saveAbout',
        'aboutText': aboutText
      };

      $.ajax({
        type: 'POST',
        dataType: 'json',
        url: 'ajax',
        data: data,
        success: function(data) {
          data = jQuery.parseJSON( data["json"] );
          //console.log(data);
        },
        error: function(request, status, error){
          //console.log('error: ' + request.responseText);
        }
      });

    });
  }
  
  $('.btn-upload').click(function(e) {
    e.preventDefault();
    var modal = $(".modal-upload");

    if(modal.hasClass('hidden')) {
      modal.removeClass('hidden');
    }    
  });

  $('.alert-close').click(function() {
    $(this).parent().fadeOut('slow', function() {
      $(this).remove();
    });
  });
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
});