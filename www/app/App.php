<?php
require_once 'core/init.php';
new Config();

if(!DEV_ENV) set_error_handler("myErrorHandler");

function myErrorHandler($errno, $errstr, $errfile, $errline) {
  $log = new Log();
  $log->write("Error[$errno] $errstr\nError on line $errline in $errfile");
}

class App{
  
  public function run() {
    $this->parseUrl();
  }
  
  private function parseUrl(){
    $url = $_SERVER['REQUEST_URI'];
    $url = trim($url, ' /');
    $url = explode('/', $url);
    
    $controller = $url !== ''    && (isset($url[0]) && $url[0] !== '')  ? $url[0] :  DEFAULT_CONTROLLER;
    $method     = $url !== ''    && isset($url[1])  ? $url[1] :  DEFAULT_METHOD;
    $args       = is_array($url) && count($url) > 2 ? array_slice($url, 2) : array();
    
    $file = dirname(__FILE__) . '/controllers/' . $controller . '.php';    
    if(file_exists($file)){
      require_once $file;
    } else {
      echo 'error 404 - page not found';
      return false;
    }
    
    $controller = new $controller;
    
    if(method_exists($controller, $method)){
      $controller->$method();
    } else {
      // die("method doesn't exist");
    }
    
  }

}