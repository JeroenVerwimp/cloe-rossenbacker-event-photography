<?php
require_once dirname(__FILE__) . '/../core/init.php';

class Contact extends Controller {
  
  public function index() {
    HitCounter::Register('contact');
    
    $error = '';
    
    if(isset($_POST['send'])){
      if(empty($_POST['name']) || empty($_POST['email']) || empty($_POST['message'])){
        $error = 'You need to fill in all fields!';
        return $error;
      }
      
      $name = $_POST['name'];
      $email = $_POST['email'];
      $message = $_POST['message'];
      
      $mail = new PHPMailer;
      $mail->isSMTP();
      $mail->Host = 'mail.jeroenver.be';
      $mail->SMTPAuth = true;
      $mail->Username = 'mail@jeroenver.be';
      $mail->Password = 'RNsHLe45';
      $mail->SMTPSecure = 'tls';
      $mail->Port = 587;
      
      $mail->From = CONTACT_SEND_EMAIL;
      $mail->FromName = 'Contact @ creventphotography.be';
      
      $db = new DB();
      $contact_forward = $db->query("SELECT value FROM settings WHERE name = :name", array("name"=>"contact_forward"));
      if(empty($contact_forward)){
        $contact_forward = 0;
      } else {
        $contact_forward = $contact_forward[0]['value'];
      }
      
      if($contact_forward == 1) {
        $contact_email = $db->query("SELECT value FROM settings WHERE name = :name", array("name"=>"contact_forward_email"));
        if(empty($contact_email)){
          $mail->addAddress(CONTACT_EMAIL);
        } else {
          $mail->addAddress($contact_email[0]['value']);
        }
      } else {
        $mail->addAddress(CONTACT_EMAIL);
      }
      
      $mail->isHTML(true); 
      $mail->Subject = 'Contact: ' . $name;
      $mail->Body    = '<div class="email-background" style="background-color: #bdc3c7;padding: 10px;"><div class="email-container" style="max-width: 500px;margin: 0 auto;background-color: white;font-family: sans-serif;border-radius: 5px;overflow: hidden;"><img src="jeroenver.be/img/mail_header.png" alt="email header" style="max-width: 100%;"><h1 style="margin: 10px;font-size: 32px;font-weight: 300;text-align: center;color: #666666;line-height: 1.5;">Contact</h1><div class="contact-info" style="width: 100%;"><div class="name" style="width: calc(100% - 20px);margin: 0 10px;"><h2 style="color: #999999;font-size: 14px;margin: 0;padding: 0 0 0 14px;font-family: Georgia, Cambria, &quot;Times New Roman&quot;, Times, serif;font-style: italic;">Name:</h2><h3 style="color: #21a1e1;font-size: 18px;padding: 15px 0;margin: 0 0 10px 0;border-bottom: 2px solid #21a1e1;">'. $name .'</h3></div><div class="email" style="width: calc(100% - 20px);margin: 0 10px;"><h2 style="color: #999999;font-size: 14px;margin: 0;padding: 0 0 0 14px;font-family: Georgia, Cambria, &quot;Times New Roman&quot;, Times, serif;font-style: italic;">Email:</h2><h3 style="color: #21a1e1;font-size: 18px;padding: 15px 0;margin: 0 0 10px 0;border-bottom: 2px solid #21a1e1;">'. $email .'</h3></div><div class="message" style="background: rgba(0, 0, 0, 0.035);width: calc(100% - 20px);margin: 0 10px;padding: 8px 0 0 0;border-top: 2px solid #21a1e1;"><h2 style="color: #999999;font-size: 14px;margin: 0;padding: 0 0 0 14px;font-family: Georgia, Cambria, &quot;Times New Roman&quot;, Times, serif;font-style: italic;">Message:</h2><h3 style="color: #21a1e1;font-size: 18px;padding: 15px 0;margin: 0 0 10px 0;">'. $message. '</h3></div></div><div class="options" style="text-align: center;margin: 20px;"><a href="mailto:me@jeroenver.be" style="text-decoration: none;letter-spacing: 0.05em;background: none;color: #21a1e1;display: inline-block;padding: 6px 10px;border: 2px solid #21a1e1;">Reply</a></div></div></div>';
      $mail->AltBody = 'name: ' . $name . '\nemail: ' . $email . '\nmessage: ' . $message . '\n\n If you see this email, I would suggest you use a mailclient that allows html-emails like gmail, Outlook, ... . Then you will be able to see a verry nice looking email!';
      
      if(!$mail->send()) {
        $error = 'Failed to send email!';
        return $error;
      }
    }
    
    if(!empty($error)) {
      echo $error;
      die();
    }
    
    View::render('contact/index', 'includes/header', 'includes/footer', $error);
  }
  
}