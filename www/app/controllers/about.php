<?php
require_once dirname(__FILE__) . '/../core/init.php';

class About extends Controller {
  
  public function index() {
    HitCounter::Register('about');
    
    View::render('about/index', 'includes/header', 'includes/footer');
  }
  
}