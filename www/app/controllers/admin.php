<?php
require_once dirname(__FILE__) . '/../core/init.php';

class Admin extends Controller {
  
  public function index(){
    if (!Login::loggedin()) {
      $this->login();
      return;
    }
    
    View::render('admin/includes/admin-header');
    View::render('admin/index');
    View::render('admin/includes/admin-footer');
  }
  
  public function about() {
    if (!Login::loggedin()) {
      $this->login();
      return;
    }
    
    View::render('admin/includes/admin-header');
    View::render('admin/about');
    View::render('admin/includes/admin-footer');
  }
  
  public function gallery(){
    if (!Login::loggedin()) {
      $this->login();
      return;
    }
    
    View::render('admin/includes/admin-header');
    View::render('admin/gallery');
    View::render('admin/includes/admin-footer');
  }

  public function galleryuploadtoalbum() {
    header('Content-Type: application/json');

    $uploaded = array();
    $failed = array();
    
    //$failed[] = $_FILES['file'];
    if(!empty($_FILES['file']['name'][0])){
      
      $db = new DB();
      
      $toAlbum = $_POST['toAlbum'];
      $albumName = (isset($_POST['albumName']) ? $_POST['albumName'] : null);
      $albumId = null;
      
      if(isset($_POST['createAlbum'])) {
        $insert = $db->query("INSERT INTO albums(name,images) VALUES(:name,:imgs)", array('name' => $albumName, 'imgs'=>'[]'));
        $albumId = $db->query("SELECT * FROM albums WHERE name = :name", array('name'=>$albumName));
        $albumId = $albumId[0]['id'];
      } else {
        if(isset($_POST['albumId'])) {
          $albumId = intval($_POST['albumId']);
        }
      }
      
      $files = $_FILES['file'];

      $allowed = array('bmp', 'gif', 'jpg', 'jpeg', 'png', 'svg');

      foreach($files['name'] as $position => $file_name) {

        $file_tmp = $files['tmp_name'][$position];
        $file_size = $files['size'][$position];
        $file_error = $files['error'][$position];

        $file_ext = explode('.', $file_name);
        $file_ext = strtolower(end($file_ext));

        if(in_array($file_ext, $allowed)) {

          if($file_error === 0) {

            if($file_size <= 262144000) { // 256 mb

              $file_name_new = uniqid('', true) . '.' . $file_ext;
              $file_destination = 'img/uploads/' . $file_name_new;

              if(move_uploaded_file($file_tmp, $file_destination)) {
                $uploaded[$position] = $file_destination;

                $insert = $db->query("INSERT INTO images(name,description,url) VALUES(:name,:desc,:url)",
                           array("name" => 'unknown',"desc" => 'unknown',"url" => $file_destination));
                
                $uploaded[] = "uploaded";
                
                if($toAlbum == "true") {
                  $image_id = $db->lastInsertId();
                  $albums = $db->query("SELECT * FROM albums WHERE id = :id LIMIT 1", array("id"=>$albumId));
                  $album_images = $albums[0]['images'];
                  
                  $imgs = json_decode($album_images);
                  $imgs[] = intval($image_id);
                  $imgs = json_encode($imgs);
                  
                  $update = $db->query("UPDATE albums SET images = :imgs WHERE id = :id ", array("id"=>$albumId,"imgs"=>$imgs));
                }
              } else {
                $failed[$position] = "[{$file_name}] failed uploading file!";
              }
            } else {
              $failed[$position] = "[{$file_name}] file is to large!";
            }
          } else {
            $failed[$position] = "[{$file_name}] errored with code {$file_error}";
          } 
        } else {
          $failed[$position] = "[{$file_name}] file extension '{$file_ext}' is not allowed.";
        }
      }
      
      //
      
    } else {
      $failed[] = 'no files';
    }
    
    $json_response = array($uploaded, $failed);
    echo json_encode($json_response);
  }
  
  public function settings(){
    if (!Login::loggedin()) {
      $this->login();
      return;
    }
    $error = array();
    
    // get post password
    
    if(isset($_POST['password'])) {
      $oldpass = $_POST['oldpass'];
      $newpass = $_POST['newpass'];

      $db = new DB();
      $db->bind('id', $_SESSION['user_id']);
      $result = $db->query("SELECT password FROM users WHERE id = :id LIMIT 1");

      if(empty($result)) {
        echo 'no result';
        die();
      }

      $password_hash = $result[0]['password'];
      if(Password::check($oldpass, $password_hash)) {
        $newpass_hash = Password::hash($newpass);
        $update = $db->query("UPDATE users SET password = :pass WHERE id = :id", array('pass' => $newpass_hash, 'id' => $_SESSION['user_id']));
        if($update === 0) {
          $error[] = 'failed to change password';
        }
      } else {
        $error[] = 'current password was incorect!';
      }
    }
    
    if(isset($_POST['share'])) {
      
      $share_facebook = (array_key_exists('share_facebook', $_POST) ? 1 : 0);
      $share_facebook = strval($share_facebook);
      $share_twitter = (array_key_exists('share_twitter', $_POST) ? 1 : 0);
      $share_twitter = strval($share_twitter);
      $share_googleplus = (array_key_exists('share_googleplus', $_POST) ? 1 : 0);
      $share_googleplus = strval($share_googleplus);
      
      $db = new DB();
      $db_share_facebook =  $db->query("SELECT value FROM settings WHERE name = :name", array("name"=>"share_facebook"));
      $db_share_facebook = $db_share_facebook[0]['value'];
      $db_share_twitter =  $db->query("SELECT value FROM settings WHERE name = :name", array("name"=>"share_twitter"));
      $db_share_twitter = $db_share_twitter[0]['value'];
      $db_share_googleplus =  $db->query("SELECT value FROM settings WHERE name = :name", array("name"=>"share_googleplus"));
      $db_share_googleplus = $db_share_googleplus[0]['value'];
      
      if($share_facebook !== $db_share_facebook) {
        $update1 = $db->query("UPDATE settings SET value = :value WHERE name = :name", array('value' => $share_facebook, 'name' => 'share_facebook'));
        
        if($update1 === 0) {
          $error[] = 'Failed to save settings! Try again later or contact a server administrator.';
        }
      }
      
      if($share_twitter !== $db_share_twitter) {
        $update2 = $db->query("UPDATE settings SET value = :value WHERE name = :name", array('value' => $share_twitter, 'name' => 'share_twitter'));
        
        if($update2 === 0) {
          $error[] = 'Failed to save settings! Try again later or contact a server administrator.';
        }
      }
      
      if($share_googleplus !== $db_share_googleplus) {
        $update3 = $db->query("UPDATE settings SET value = :value WHERE name = :name", array('value' => $share_googleplus, 'name' => 'share_googleplus'));
        
        if($update3 === 0){
          $error[] = 'Failed to save settings! Try again later or contact a server administrator.';
        }
      }
      
    }
    
    if(isset($_POST['contact'])) {
      $forward = strval((array_key_exists('contact_forward', $_POST) ? 1 : 0)); //$forward = strval($forward);
      $contact_email = $_POST['contact_email'];
      
      $db = new DB();
      $db_contact_forward =  $db->query("SELECT value FROM settings WHERE name = :name", array("name"=>"contact_forward"));
      $db_contact_forward = $db_contact_forward[0]['value'];
      $db_contact_email = $db->query("SELECT value FROM settings WHERE name = :name", array("name"=>"contact_forward_email"));
      $db_contact_email = $db_contact_email[0]['value'];
      
      if($forward !== $db_contact_forward) {
        $update1 = $db->query("UPDATE settings SET value = :value WHERE name = :name", array('value' => $forward, 'name' => 'contact_forward'));
        
        if($update1 === 0) {
          $error[] = 'Failed to save settings! Try again later or contact a server administrator.';
        }
      }
      
      if($contact_email !== $db_contact_email) {
        $update2 = $db->query("UPDATE settings SET value = :value WHERE name = :name", array('value' => $contact_email, 'name' => 'contact_forward_email'));
        
        if($update2 === 0) {
          $error[] = 'Failed to save settings! Try again later or contact a server administrator.';
        }
      }
      
    }
    
    View::render('admin/settings', 'admin/includes/admin-header', 'admin/includes/admin-footer', $error);
  }
  
  public function login() {
    Login::login();
    View::render('admin/login');
  }
  
  public function logout() {
    if(Login::loggedin()) {
      Login::logout();
    }
  }
  
  public function ajax() {
    if($this->is_ajax()) {
      if(isset($_POST["action"]) && !empty($_POST["action"])) {
        $action = $_POST["action"];
        switch($action) {
          case 'allImages': $this->ajaxGetAllImages(); break;
          case 'albumData': $this->ajaxAlbumData(); break;
          case 'imageData': $this->ajaxImageData(); break;
          case 'saveAbout': $this->ajaxSaveAbout(); break;
          case 'saveAlbumData': $this->ajaxSaveAlbumData(); break;
          case 'deleteImages': $this->ajaxDeleteImages(); break;
          case 'deleteAlbum': $this->ajaxDeleteAlbum(); break;
        }
      }
    }
  }
  
  private function is_ajax() {
    return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
  }
  
  private function ajaxGetAllImages() {
    if(!Login::loggedin()) return;
    
    $return = $_POST;
    
    $db = new DB();
    $images = $db->query("SELECT * FROM images");
    
    $return['images'] = $images;
    $return['json'] = json_encode($return);
    echo json_encode($return);
  }
  
  private function ajaxAlbumData() {
    $return = $_POST;
    $albumId = $return['albumId'];
    
    $db = new DB();
    $result = $db->query("SELECT * FROM albums WHERE id = :id LIMIT 1", array('id'=>$albumId));
    
    $return['albumData'] = $result[0];
    
    $images = array();
    $image_ids = json_decode($result[0]['images']);
    if(!empty($image_ids)) {
      $imageIds = implode(',', json_decode($result[0]['images']));
      $images = $db->query("SELECT * FROM images WHERE id IN({$imageIds}) ORDER BY FIELD(id, {$imageIds})");
    } else {
      $imageIds = '0';
    }
    
    $unusedImages = $db->query("SELECT * FROM images WHERE id NOT IN({$imageIds})");
    
    $return['albumImages'] = $images;
    $return['unusedImages'] = $unusedImages;
    
    $return['json'] = json_encode($return);
    echo json_encode($return);
  }
  
  private function ajaxImageData() {
    $return = $_POST;
    $imageId = $return['imageId'];
    
    $db = new DB();
    $result = $db->query("SELECT * FROM images WHERE id = :id LIMIT 1", array('id'=>$imageId));
    
    $return['imageData'] = $result[0];
    
    $return['json'] = json_encode($return);
    echo json_encode($return);
  }
  
  private function ajaxSaveAlbumData() {
    if(!Login::loggedin()) return;
    
    $return = $_POST;
    
    $albumId = $return['albumId'];
    $albumName = $return['albumName'];
    $albumHidden = $return['albumHidden'];
    $albumImages = $return['albumImages'];
    $albumThumb = $return['albumThumb'];
    
    $db = new DB();
    $db->query('UPDATE albums SET name = :name, hidden = :hidden, images = :images, thumb_image = :thumb_image WHERE id = :id', 
                array('id' => $albumId, 'name' => $albumName, 'hidden' => filter_var($albumHidden, FILTER_VALIDATE_BOOLEAN), 'images' => $albumImages, 'thumb_image' => $albumThumb));
    
    
    $return['json'] = json_encode($return);
    
    echo json_encode($return);
  }
  
  private function ajaxSaveAbout() {
    if(!Login::loggedin()) return;
    
    $return = $_POST;
    $aboutText = $return['aboutText'];
    
    $db = new DB();
    $update = $db->query("UPDATE settings SET value = :value WHERE name = :name", array("name"=>"page_about","value"=>$aboutText));
    
    $return['returnText'] = $aboutText;
    $return['json'] = json_encode($return);
    echo json_encode($return);
  }
  
  private function ajaxDeleteImages() {
    if(!Login::loggedin()) return;
    
    $return = $_POST;
    $imageIds = json_decode($return['imageIds']);
      
    $db = new DB();
    foreach($imageIds as $id) {
      $id = intval($id);
      $delete = $db->query("DELETE FROM images WHERE id = :id", array("id"=>$id));

      $albums = $db->query("SELECT * FROM albums");
      foreach($albums as $album) {
        $album_images = $album['images'];

        $imgs = json_decode($album_images);

        $keysToRemove = array_keys($imgs, $id);
        foreach($keysToRemove as $k) {
          unset($imgs[$k]);
        }

        $imgs = array_values($imgs);
        $imgs = json_encode($imgs);

        $update = $db->query("UPDATE albums SET images = :imgs WHERE id = :id ", array("id"=>$album['id'],"imgs"=>$imgs));
      }
    }
    unset($id);
    
    $return['json'] = json_encode($return);
    echo json_encode($return);
  }
  
  private function ajaxDeleteAlbum() {
    if(!Login::loggedin()) return;
    
    $return = $_POST;
    $albumId = $return['albumId'];
    
    $db = new DB();
    $db->query('DELETE FROM albums WHERE id = :id', array('id' => $albumId));
    
    $return['json'] = json_encode($return);
    echo json_encode($return);
  }
}
























