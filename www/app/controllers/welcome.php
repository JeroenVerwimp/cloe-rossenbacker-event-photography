<?php
require_once dirname(__FILE__) . '/../core/init.php';

class Welcome extends Controller {
  
  public function index() {
    HitCounter::Register('home');
    
    View::render('home/index', 'includes/header', 'includes/footer');
  }
  
}