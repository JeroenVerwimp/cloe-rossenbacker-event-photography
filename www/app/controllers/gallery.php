<?php
require_once dirname(__FILE__) . '/../core/init.php';

class Gallery extends Controller {
  
  public function index() {
    HitCounter::Register('gallery');
    
    View::render('gallery/index', 'includes/header', 'includes/footer');
  }
  
}