<?php

class Config {
  
  public function __construct() {
    
    // default controller and method
    define('DEFAULT_CONTROLLER', 'welcome');
    define('DEFAULT_METHOD' , 'index');
    
    // set site title
    define('SITETITLE', 'Cloe Rossenbacker eventphotography');
    
    define('DEFAULT_LANG', 'en');
    
    // contact
    define('CONTACT_EMAIL', 'contact@creventphotography.be');
    define('CONTACT_SEND_EMAIL', 'mail@creventphotography.be');
    
    
    if($_SERVER['HTTP_HOST'] === 'www.creventphotography.be' || $_SERVER['HTTP_HOST'] === 'creventphotography.be') {
      define('DEV_ENV', false);
    } else {
      define('DEV_ENV', true);
    }
    
    
  }
  
}