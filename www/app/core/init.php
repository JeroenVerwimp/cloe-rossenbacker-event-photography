<?php

// Core
// ----------------------------------------
require_once dirname(__FILE__) . '/config.php';
require_once dirname(__FILE__) . '/controller.php';
require_once dirname(__FILE__) . '/view.php';

// Helpers
// ----------------------------------------
require_once dirname(__FILE__) . '/handlers/Log.class.php';
require_once dirname(__FILE__) . '/handlers/DB.php';
require_once dirname(__FILE__) . '/handlers/Password.php';
require_once dirname(__FILE__) . '/handlers/Language.php';
require_once dirname(__FILE__) . '/handlers/Login.php';
require_once dirname(__FILE__) . '/handlers/HitCounter.php';

// Libs
// ----------------------------------------
require_once dirname(__FILE__) . '/PHPMailer/PHPMailerAutoload.php';


//Lang::setLang('en');