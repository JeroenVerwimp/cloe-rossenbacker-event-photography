<?php

class Password {
  
  public static function hash($password){
    $salt = openssl_random_pseudo_bytes(22);
    $salt = '$2a$%13$' . strtr(base64_encode($salt), array('_' => '.', '~' => '/'));
    $password_hash = crypt($password, $salt);
    return $password_hash;
  }
  
  public static function check($password, $password_hash) {
    if($password_hash === crypt($password, $password_hash)) {
      return true;
    } else {
      return false;
    }
  }
}
  