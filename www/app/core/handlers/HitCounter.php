<?php

class HitCounter {
  const HIT_OLD_AFTER_SECONDS = 2592000; // 30 days
  const IGNORE_SEARCH_BOTS = true;
  const HONOR_DO_NOT_TRACK = false;
  
  private static $IP_IGNORE_LIST = array(
    '127.0.0.1',
    '10.0.2.2'
  );
  
  private static $DB = false;
  
  private static function InitDB() {
    if(self::$DB)
      return;
    
    self::$DB = new DB();
  }
  
  public static function Register($pageID) {
    if(self::IGNORE_SEARCH_BOTS && self::IsSearchBot())
      return false;
    if(in_array($_SERVER['REMOTE_ADDR'], self::$IP_IGNORE_LIST))
      return false;
    if(self::HONOR_DO_NOT_TRACK && isset($_SERVER['HTTP_DNT']) && $_SERVER['HTTP_DNT'] == "1")
      return false;
    
    self::InitDB();
    
    self::Cleanup();
    self::CreateCountsIfNotPresent($pageID);
    
    if(self::UniqueHit($pageID)) {
      self::CountHit($pageID, true);
      self::LogHit($pageID);
    }
    self::CountHit($pageID, false);
    
    return true;
  }
  
  public static function GetHits($pageID, $unique = false) {
    self::InitDB();
    self::CreateCountsIfNotPresent($pageID);
    
    $q = self::$DB->query('SELECT hitcount FROM hit_hits WHERE pageid = :pageid AND isunique = :isunique', array('pageid'=>$pageID, 'isunique'=>$unique));
    
    if($q !== false) {
        return intval($q[0]['hitcount']);
    } else {
        return false;
    }
  }
  
  public static function GetTotalHits($unique = false) {
    self::InitDB();
    $q = self::$DB->query('SELECT hitcount FROM hit_hits WHERE isunique = :isunique', array('isunique'=>$unique));
    
    $total = 0;
    foreach($q as $row) {
      $total += intval($row['hitcount']);
    }
    
    return $total;
  }
  
  /*====================== PRIVATE METHODS =============================*/
  private static function IsSearchBot() {
    $keywords = array(
      'bot',
      'spider',
      'spyder',
      'crawlwer',
      'walker',
      'search',
      'yahoo',
      'holmes',
      'htdig',
      'archive',
      'tineye',
      'yacy',
      'yeti'
    );

    $agent = strtolower($_SERVER['HTTP_USER_AGENT']);
    foreach($keywords as $keyword) {
      if(strpos($agent, $keyword) !== false)
        return true;
    }
    
    return false;
  }
  
  private static function UniqueHit($pageID){
    $ids_hash = self::IDHash($pageID);
    $q = self::$DB->query('SELECT time FROM hit_nodupes WHERE ids_hash = :ids_hash', array('ids_hash'=>$ids_hash));
    
    if(count($q) !== 0) {
      if($q[0]['time'] > time() - self::HIT_OLD_AFTER_SECONDS)
        return false;
      else
        return true;
    } else {
        return true;
    }
  }
  
  private static function LogHit($pageID) {
    $ids_hash = self::IDHash($pageID);
    $q = self::$DB->query('SELECT time FROM hit_nodupes WHERE ids_hash = :ids_hash', array('ids_hash'=>$ids_hash));
    $curTime = time();
    
    if(count($q) !== 0){
      $s = self::$DB->query('UPDATE hit_nodupes SET time = :time WHERE ids_hash = :ids_hash', array('time'=>$curTime, 'ids_hash'=>$ids_hash));
    } else {
      $s = self::$DB->query('INSERT INTO hit_nodupes(ids_hash,time) VALUES(:ids_hash,:time)', array('time'=>$curTime, 'ids_hash'=>$ids_hash));
    }
  }
  
  private static function CountHit($pageID, $unique) {
    $unique = ($unique ? '1' : '0');
    $q = self::$DB->query('UPDATE hit_hits SET hitcount = hitcount + 1 WHERE pageid = :pageid AND isunique = :isunique', array('pageid'=>$pageID, 'isunique'=>$unique));
  }
  
  private static function IDHash($pageID) {
    $visitorID = $_SERVER['REMOTE_ADDR'];
    return hash("SHA256", $pageID . $visitorID);
  }
  
  private static function CreateCountsIfNotPresent($pageID) {
    // Non-unique
    $q = self::$DB->query('SELECT pageid FROM hit_hits WHERE pageid = :pageid AND isunique = 0', array('pageid'=>$pageID));
    
    if(count($q) === 0) {
      $s = self::$DB->query('INSERT INTO hit_hits (pageid, isunique, hitcount) VALUES (:pageid, 0, 0)', array('pageid'=>$pageID));
    }
    
    // Unique
    $q = self::$DB->query('SELECT pageid FROM hit_hits WHERE pageid = :pageid AND isunique = 1', array('pageid'=>$pageID));
    
    if(count($q) === 0) {
      $s = self::$DB->query('INSERT INTO hit_hits (pageid, isunique, hitcount) VALUES (:pageid, 1, 0)', array('pageid'=>$pageID));
    }
  }
  
  private static function Cleanup() {
    $last_interval = time() - self::HIT_OLD_AFTER_SECONDS;
    self::$DB->query("DELETE FROM hit_nodupes WHERE time < :time", array('time'=>$last_interval));
  }
  
}

























