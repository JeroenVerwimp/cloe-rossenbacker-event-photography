<?php

class Lang {

  private static $userLang = DEFAULT_LANG;
  private static $lang = array();
  
  public static function setLang($ulang){
    $userLang = $ulang;
    $langFile = dirname(__FILE__) . '/../../lang/' . $userLang . '.ini';
    
    if(!file_exists($langFile)){
      $log = new Log();
      $log->write('[Language] Language file not found switching to default: ' . DEFAULT_LANG);
      $langFile = dirname(__FILE__) . '/../../lang/' . DEFAULT_LANG . '.ini';
    }
    self::$lang = parse_ini_file($langFile);
    //var_dump($lang);
    //die();
  }
  
  public static function get($key, $arg = array()) {
    $local = self::$lang[$key];
    return $local;
  }
  
}
