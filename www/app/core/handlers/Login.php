<?php

session_start();

class Login {
  public function __construct() {}
  
  public static function login() {
    $error = '';
    
    if(isset($_POST['submit'])){
      if(empty($_POST['username']) || empty($_POST['password'])){
        $error = 'Username or password is invalid';
        return $error;
      }
      
      $username = $_POST['username'];
      $password = $_POST['password'];
      
      $db = new DB();
      $db->bind('username', $username);
      $result = $db->query("SELECT id, password FROM users WHERE username = :username LIMIT 1");
      
      if(empty($result)){
        echo 'db error: no result';
        die();
      }
      
      $id = $result[0]['id'];
      $password_hash = $result[0]['password'];
      
      if (Password::check($password, $password_hash)) {
        //current
        $_SESSION['user_id'] = $id;
        header('Location: '.$_SERVER['REQUEST_URI']);
      } else {
        $error = 'Username or password is invalid';
        return $error;
      }
    }
    
  }
  
  public static function loggedin() {
    if(isset($_SESSION['user_id'])) {
      return true;
    } else {
      return false;
    }
  }
  
  public static function logout() {
    //session_start();
    if(session_destroy()) {
      header('Location: /admin');
    }
  }

}