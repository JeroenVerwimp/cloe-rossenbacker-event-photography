<?php

class View {
  
  public static function render($path, $header = '', $footer = '', $error = array()) {
    $_SESSION['error'] = $error;
    
    if($header !== '') {
      include dirname(__FILE__) . "/../views/$header.php";
    }
    
    require_once dirname(__FILE__) . "/../views/$path.php";
    
    if($footer !== '') {
      include dirname(__FILE__) . "/../views/$footer.php";
    }
  }
  
}