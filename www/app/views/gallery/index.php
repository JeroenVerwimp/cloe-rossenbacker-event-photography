<?php
$db = new DB;
$albums = $db->query("SELECT * FROM albums");

?>
<div class="page-content">
  <div class="gallery-page-wrapper">
    <div class="gallery-wrapper">
      <h1>Gallery</h1>

      <div class="thumb-container">
        <?php

          foreach($albums as $album) {
            if($album['hidden'] == 1) {
              continue;
            }

            $album_images = json_decode($album['images']);
            $thumb_image_url = 'img/header.jpg';
            $thumb_image_id = ($album['thumb_image'] == 0 ? null : $album['thumb_image']);
            if(isset($album_images[0]) && $thumb_image_id == null) {
              $thumb_image_id = $album_images[0];
            }

            $thumb_image = $db->query("SELECT * FROM images WHERE id = :id LIMIT 1", array("id" => $thumb_image_id));
            if(isset($thumb_image[0])) {
              $thumb_image_url = $thumb_image[0]['url'];
            }

            echo '<a href="#" class="thumb-unit" data-id="', $album['id'], '" style="background-image: url(', $thumb_image_url ,');">';
            echo '<div class="thumb-overlay">';
            echo '<strong>', $album['name'], '</strong>';
            echo '<span class="more-icon"></span>';
            echo '</div>';
            echo '</a>';


          }

        ?>

      </div>

    </div>
    <div class="gallery-album-wrapper">
      <a href="#" class="back"></a>
      <h1>TITLE</h1>
      <div class="album-carousel">
        <ul class="images" style="margin-left: 0px;">
          <li><div class="album-carousel-image-container"><img src="img/uploads/5554c41b11fa98.71433374.jpg" alt="" class="album-carousel-image"></div></li>
          <li><div class="album-carousel-image-container"><img src="img/uploads/5522760cd564a6.26803252.jpg" alt="" class="album-carousel-image"></div></li>
        </ul>
        <div class="prev"><span>&lsaquo;</span></div>
        <div class="next"><span>&rsaquo;</span></div>
      </div>
<!--
      <div class="album-mini-carousel">
        <ul>
          <li><img src="img/uploads/5554c41b11fa98.71433374.jpg" alt=""></li>
          <li><img src="img/uploads/5522760cd564a6.26803252.jpg" alt=""></li>
        </ul>
      </div>
-->
    </div>
  </div>
</div>