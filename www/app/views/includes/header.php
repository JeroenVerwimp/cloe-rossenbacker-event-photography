<!DOCTYPE html>
<html lang="en">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta charset="UTF-8">
  <title><?php echo SITETITLE; ?></title>
  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="../css/icons.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="../js/jquery.jcarousel.min.js"></script>
  <script src="../js/script.min.js"></script>
</head>
<body>
  <div class="page-wrap">
    <div class="menu">
      <header>
        <h1>Cloë Rossenbacker eventphotography</h1>
      </header>
      <ul class="nav">
        <li><a href="/">Home</a></li>
        <li><a href="/about">About me</a></li>
        <li><a href="/gallery">Gallery</a></li>
        <li><a href="/contact">Contact</a></li>
      </ul>
      
      <div class="social">
        <a href="https://www.facebook.com/cloerossenbackereventphotography" class="facebook"><span class="socicon socicon-facebook"></span></a>
      </div>
    </div>