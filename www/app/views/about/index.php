<div class="page-content">
  <div class="about-wrapper">
    <h1>About</h1>
    <div class="info">
       <?php
      $db = new DB;
      $page_about =  $db->query("SELECT value FROM settings WHERE name = :name", array("name"=>"page_about"));
      $page_about = $page_about[0]['value'];
      
      echo $page_about;
      ?>
    </div>
  </div>
</div>