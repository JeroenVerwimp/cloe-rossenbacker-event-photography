<?php
require dirname(__FILE__) . '/../../core/init.php';
$db = new DB;

$carousel_album = $db->query("SELECT * FROM albums WHERE name = :name LIMIT 1", array('name' => 'carousel'));
$image_ids = implode(',', json_decode($carousel_album[0]['images']));
$carousel_images = $db->query("SELECT * FROM images WHERE id IN({$image_ids}) ORDER BY FIELD(id, {$image_ids})");

//var_dump($carousel_images);
?>

<div class="page-content">
  
  <div class="album-carousel album-carousel-main">
    <ul class="images" style="margin-left: 0px;">
      <?php
        foreach($carousel_images as $image) {
          echo '<li><div class="album-carousel-image-container"><img src="../', $image['url'], '" alt="" class="album-carousel-image"></div></li>';
        }
      ?>
    </ul>
    <div class="prev"><span>&lsaquo;</span></div>
    <div class="next"><span>&rsaquo;</span></div>
  </div>
  
</div>