<div class="page-content">
  
  <div class="contact-wrapper">
    <h1>Contact</h1>
    <form class="contact-form" action="contact" method="post" autocomplete="off">
      <div class="input-group">
        <input name="name" type="text">
        <label>Your Full Name</label>
      </div>
      
      <div class="input-group">
        <input name="email" type="email">
        <label>Your Email Address</label>
      </div><br>
      
      <div class="input-group">
        <textarea name="message" cols="86" rows="10"></textarea>
        <label>Your Message</label>
      </div><br>
      
      <input name="send" type="submit" value="Send">
    </form>
  </div>
  
</div>