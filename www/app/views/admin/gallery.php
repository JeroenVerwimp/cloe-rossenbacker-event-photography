<?php
require dirname(__FILE__) . '/../../core/init.php';
$db = new DB;

$images = $db->query("SELECT * FROM images");
$albums = $db->query("SELECT * FROM albums");
?>
<div class="admin-content">
  <h2 class="admin-title">Gallery</h2>
  
  <div class="toolbar">
    <div class="left">
<!--      <a href="#" class="btn btn-green btn-addalbum">Add Album</a>-->
      <a href="#" class="btn btn-red btn-delete">Delete selected images</a>
    </div>
    <div class="right">
      <a href="#" class="btn btn-green btn-upload">Upload</a>
    </div>
  </div>
  
  <h3>Albums</h3>
  <div class="albums">
    <?php 
    
    foreach($albums as $album) {
      echo '<div id="album-container-', $album['id'],'" class="album-container">';
      echo '<a class="btn btn-orange btn-album-edit" data-album-id="', $album['id'],'">Edit</a>';
      echo '<div class="overlay"></div>';
      echo '<div class="icons">';
      echo ($album['hidden'] == 1) ? '<img src="../img/blind-eye.png" alt="hidden">' : '';
      echo ($album['deletable'] == 0) ? '<img src="../img/locked.png" alt="">' : '';
      echo '</div>';
      
      $album_images = json_decode($album['images']);
      $thumb_image_url = 'img/header.jpg';
      $thumb_image_id = ($album['thumb_image'] == 0 ? null : $album['thumb_image']);
      if(isset($album_images[0]) && $thumb_image_id == null) {
        $thumb_image_id = $album_images[0];
      }
      
      $thumb_image = $db->query("SELECT * FROM images WHERE id = :id LIMIT 1", array("id" => $thumb_image_id));
      if(isset($thumb_image[0])) {
        $thumb_image_url = $thumb_image[0]['url'];
      }
      echo '<img src="../', $thumb_image_url, '" alt="">';
      echo '</div>';
    }
    
    ?>
  </div>
  <h3>Images</h3>
  <div class="images">
    <?php 
    
    foreach($images as $image) {
      echo '<div id="image-container-', $image['id'],'" class="image-container" data-image-id="', $image['id'],'">';
      echo '<label>';
      echo '<input id="checkbox-image-', $image['id'],'" type="checkbox">';
      echo '<div class="overlay"></div>';
      echo '<div class="icons">';
      echo ($image['hidden'] == 1) ? '<img src="../img/blind-eye.png" alt="hidden">' : '';
      echo ($image['deletable'] == 0) ? '<img src="../img/locked.png" alt="">' : '';
      echo '</div>';
      echo '<img src="../', $image['url'], '" alt="">';
      echo '</label>';
      echo '</div>';
    }
    
    ?>
  </div>
</div>

<div id="modal-upload" class="modal-upload hidden">
  <a id="modal-upload-exit" class="modal-upload-exit">&#10005;</a>
  <form action="" method="post">
    <div id="dropzone" class="dropzone">
      <h1 id="dropzone_title">Drop files here to upload</h1>
    </div>
    <div class="album">
      <label>
        <input type="checkbox" id="upload-add-to-album">
        add To album
      </label>
      <select name="album" id="selectalbum" disabled>
        <?php 
    
        foreach($albums as $album) {
          echo '<option value="', $album['id'], '">',$album['name'], '</option>';
        }

        ?>
        <option value="4586_createalbum">New Album</option>
      </select><br>
      <div id="upload-to-album-info" style="display: none">
        name:
        <input id="upload-to-album-name" type="text">
      </div>
    </div>
    <div class="manual">
      <label for="dropzone_select_manual">Select<input id="dropzone_select_manual" class="upload" type="file" multiple /></label>      
      <a id="dropzone_upload">Upload</a>
    </div>
  </form>
</div>

<div id="modal-addalbum" class="modal-addalbum hidden">
  <a id="modal-addalbum-exit" class="modal-addalbum-exit">&#10005;</a>
  <form>
    <input id="modal-addalbum-data-id" type="text" style="display: none;">
    <label>
      name:
      <input id="modal-addalbum-data-name" type="text">
    </label>
    <label>
      hidden:
      <input id="modal-addalbum-data-hidden" type="checkbox">
    </label>
    <div class="right">
      <a id="btn-make-thumb" href="#" class="btn btn-red">Make thumbnail</a>
    </div>
    <div class="images">
      <div class="images1"></div>
      <div class="images2"></div>
      <div class="move-container">
        <a href="#" class="move move-right">&raquo;</a>
        <a href="#" class="move move-left">&laquo;</a>
      </div>
    </div>
    <a id="btn-album-delete" href="#" class="btn btn-red">Delete</a>
    <a id="btn-save" href="#" class="btn btn-green">save</a>
  </form>
</div>











