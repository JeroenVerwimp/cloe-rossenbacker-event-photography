<div class="admin-content">
  <div class="admin-about-wrapper">
    <h1>About</h1>
    <div id="aboutText" class="info" contentEditable="true">
      <?php
      $db = new DB;
      $page_about =  $db->query("SELECT value FROM settings WHERE name = :name", array("name"=>"page_about"));
      $page_about = $page_about[0]['value'];
      
      echo $page_about;

      ?>
    </div>
    <a href="#" class="btn btn-red btn-save-about">Save</a>
  </div>
</div>