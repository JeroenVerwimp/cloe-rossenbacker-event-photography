<div class="admin-content">
  <h2 class="admin-title">Dashboard</h2>
  
  <div class="dash-row">
    <div class="dash-panel dash-panel-red">
      <div class="dash-panel-body">
        <span class="txt">Total Views</span>
        <span class="count"><?php echo HitCounter::GetTotalHits(); ?></span>
        <span class="icon icon-male"></span>
      </div>
    </div>
    <div class="dash-panel dash-panel-green">
      <div class="dash-panel-body">
        <span class="txt">Total Unique Views</span>
        <span class="count"><?php echo HitCounter::GetTotalHits(true); ?></span>
        <span class="icon icon-male"></span>
      </div>
    </div>
  </div>
</div>