<?php
require dirname(__FILE__) . '/../../core/init.php';

$db = new DB;

$share_facebook =  $db->query("SELECT value FROM settings WHERE name = :name", array("name"=>"share_facebook"));
$share_facebook = $share_facebook[0]['value'];

$share_twitter =  $db->query("SELECT value FROM settings WHERE name = :name", array("name"=>"share_twitter"));
$share_twitter = $share_twitter[0]['value'];

$share_googleplus =  $db->query("SELECT value FROM settings WHERE name = :name", array("name"=>"share_googleplus"));
$share_googleplus = $share_googleplus[0]['value'];

$contact_forward = $db->query("SELECT value FROM settings WHERE name = :name", array("name"=>"contact_forward"));
$contact_forward = $contact_forward[0]['value'];

$contact_email = $db->query("SELECT value FROM settings WHERE name = :name", array("name"=>"contact_forward_email"));
$contact_email = $contact_email[0]['value'];

?>
<div class="admin-content">
  <h2 class="admin-title">Settings</h2>
  
  <?php if($_SESSION['error'] !== array()) : ?>
    <?php foreach($_SESSION['error'] as $message): ?>
      <div class="alert">
        <span class="alert-close">&#10006;</span>
        <?php echo $message; ?>
      </div>
    <?php endforeach; ?> 
  <?php endif; ?>
    
  <div class="row">
    <div class="col-2">
      <div class="panel">
        <div class="panel-head">
          Change Password
        </div>
        <div class="panel-body">
          <form action="" method="post">
            <input class="password" type="password" placeholder="old password" name="oldpass">
            <input class="password" type="password" placeholder="new password" name="newpass"><br>
            <input type="submit" value="save" name="password">
          </form>
        </div>
      </div>
    </div>
    
    <div class="col-2">
      <div class="panel">
        <div class="panel-head">
          Share
        </div>
        <div class="panel-body">
          <form action="" method="post">
            <div class="onoffswitch-container">
              Facebook
              <div class="onoffswitch">
                <input type="checkbox" name="share_facebook" class="onoffswitch-checkbox" id="share_facebook" <?php if($share_facebook == 1): ?> checked <?php endif ?> >
                <label for="share_facebook" class="onoffswitch-label">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
            
            <div class="onoffswitch-container">
              Twitter
              <div class="onoffswitch">
                <input type="checkbox" name="share_twitter" class="onoffswitch-checkbox" id="share_twitter" <?php if($share_twitter == 1): ?> checked <?php endif ?> >
                <label for="share_twitter" class="onoffswitch-label">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
            
            <div class="onoffswitch-container">
              Google +
              <div class="onoffswitch">
                <input type="checkbox" name="share_googleplus" class="onoffswitch-checkbox" id="share_googleplus" <?php if($share_googleplus == 1): ?> checked <?php endif ?> >
                <label for="share_googleplus" class="onoffswitch-label">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
            
            <input type="submit" value="Save" name="share">
          </form>
        </div>
      </div>
    </div>
    
    <div class="col-2">
      <div class="panel">
        <div class="panel-head">
          Contact forward
        </div>
        <div class="panel-body">
          <form action="" method="post">
            <div class="onoffswitch-container">
              Forward:
              <div class="onoffswitch">
                <input type="checkbox" name="contact_forward" class="onoffswitch-checkbox" id="contact_forward" <?php if($contact_forward == 1): ?> checked <?php endif ?> >
                <label for="contact_forward" class="onoffswitch-label">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>

            <input type="email" placeholder="email" name="contact_email" <?php if($contact_email != null){ echo 'value="', $contact_email, '"'; } ?>><br>
            <input type="submit" value="Save" name="contact">
          </form>
        </div>
      </div>
    </div>
  </div>
  
</div>