<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><?php echo 'Admin - ', SITETITLE; ?></title>
  <link rel="stylesheet" href="../css/lib/jquery.mCustomScrollbar.min.css">
  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="../css/icons.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="../js/lib/jquery.mCustomScrollbar.concat.min.js"></script>
  <script src="../js/script.js"></script>
  <script src="../js/jquery.admin.js"></script>
  <script src="../js/lib/ckeditor/ckeditor.js"></script>
</head>
<body>
  <input type="checkbox" id="sidebartoggler" checked>
  
  <div class="admin-wrap">
    <div class="admin-page-content">
      <div class="admin-header">
        <label for="sidebartoggler" class="toggle">&#9776;</label>
      </div>