    </div>
    <div class="admin-sidebar">
      <div class="admin-header">
        <div class="logo">CR EventPhotography</div>
      </div>
      <ul class="nav">
        <li><a href="/admin">Dashboard</a></li>
        <li><a href="/admin/about">About</a></li>
        <li><a href="/admin/gallery">Gallery</a></li>
        <li><a href="/admin/settings">Settings</a></li>
        <li><a href="/admin/logout">Logout</a></li>
      </ul>
    </div>
    <div class="upload-overlay">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>
  </div>
</body>
</html>