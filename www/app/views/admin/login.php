<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><?php echo 'Admin - ', SITETITLE; ?></title>
  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="../css/icons.css">
  
</head>
<body>
  
  <div class="login">
    <div class="login-header">
      <h1>Cloë Rossenbacker Event Photography</h1>
    </div>
    <form action="" method="post">
      <input type="text" name="username" placeholder="username">
      <input type="password" name="password" placeholder="password">
      
      <input type="submit" name="submit" value="login">
    </form>
  </div>
  
</body>
</html>